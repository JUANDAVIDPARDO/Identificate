<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class CityController extends Controller
{

    public function getListCity(Request $request){
        try{

            $citys = DB::select('SELECT data_location.list_city_country('.$request->country.')');
            if(empty($citys) ){

                return  response()->json([
                    'status','no hay registro'
                ],400);
            }else{

                $json = json_decode($citys[0]->list_city_country);
                return  response()->json([
                    $json
                ],200);
            }

        }catch (\Exception $e){
            return response()->json(['status'=>$e->getMessage()],400);
        }

    }

    public function getListCountry(Request $request){
        try{

            $countrys = DB::select('SELECT data_location.list_country()');
            if(empty($countrys) ){

                return  response()->json([
                    'status','no hay registro'
                ],400);
            }else{

                $json = json_decode($countrys[0]->list_country);
                return  response()->json([
                    $json
                ],200);
            }

        }catch (\Exception $e){
            return response()->json(['status'=>$e->getMessage()],400);
        }

    }

}
