<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class checkCitizenController extends Controller
{
    public function checkCitizen(Request $request){
        try{

            $citizen = DB::select('SELECT * FROM citizen WHERE card = \''.$request->card.'\'');

            if(empty($citizen) ){

                return  response()->json([
                    'status','no hay registro'
                ],400);
            }else{
                return  response()->json([

                    'citizen'=>$citizen,

                ],200);
            }



        }catch (\Exception $e){
            return response()->json(['status'=>$e->getMessage()],400);
        }

    }

    public function listAllCitizen(){
        try{

            $citizen = DB::select('SELECT * FROM citizen');

            if(empty($citizen) ){

                return  response()->json([
                    'status','no hay registro'
                ],400);
            }else{
                return  response()->json([

                    'citizen'=>$citizen,

                ],200);
            }



        }catch (\Exception $e){
            return response()->json(['status'=>$e->getMessage()],400);
        }

    }

    public function editCitizen(Request $request){
        try{

            $citizen = DB::select('INSERT INTO citizen (card,
                                    first_name,
                                    last_name,
                                    expedition_date,
                                    expedition_place,
                                    birthday,
                                    birthplace,
                                    blood_group,
                                    rh,
                                    height,
                                    required) VALUES (\''.$request->card.'\',
                                    \''.$request->first_name.'\',
                                    \''.$request->last_name.'\',
                                    DATE \''.$request->expedition_date.'\',
                                    \''.$request->expedition_place.'\',
                                    DATE \''.$request->birthday.'\',
                                    \''.$request->birthplace.'\',
                                    \''.$request->blood_group.'\',
                                    \''.$request->rh.'\',
                                    '.$request->height.',
                                    \''.$request->required.'\')
                                    ON CONFLICT  (card) DO UPDATE
                                    SET card = \''.$request->card.'\',
                                        first_name = \''.$request->first_name.'\',
                                        last_name = \''.$request->last_name.'\',
                                        expedition_date = DATE \''.$request->expedition_date.'\',
                                        expedition_place = \''.$request->expedition_place.'\',
                                        birthday = DATE \''.$request->birthday.'\',
                                        birthplace = \''.$request->birthplace.'\',
                                        blood_group = \''.$request->blood_group.'\',
                                        rh = \''.$request->rh.'\',
                                        height = '.$request->height.',
                                        required = \''.$request->required.'\'');


                return  response()->json([

                    'status'=>'success',

                ],200);
            



        }catch (\Exception $e){
            return response()->json(['status'=>$e->getMessage()],400);
        }

    }

}
