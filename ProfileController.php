<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Firebase\JWT\JWT;
use Sentinel;
use Illuminate\Support\Facades\DB;
class ProfileController extends Controller
{


    public function getProfile(Request $request){
        try{
            $user = $request->user();

            $profile = DB::select('select * from getptofile(61)');
            if(empty($profile) ){

                return  response()->json([
                    'status','no hay registro'


                ],400);
            }else{
                return  response()->json([

                    'profile'=>$profile,

                ],200);
            }



        }catch (\Exception $e){
            return response()->json(['status'=>$e->getMessage()],400);
        }

    }
    public function storeProfile(Request $request){

        try{
            $user = $request->user();

            $respuesta = DB::select('select setprofile('.$user->id.','.$request->StreetId.','.$request->numberStreet.','.$request->numberStreet2.','.$request->numberStreet3.','.$request->descriptionStreet.','.$request->cellphone.','.$request->telephone.','.$request->photo.','.$request->sexId.','.$request->birthday.','.$request->schoolId.') ');
            if($respuesta[0] == 'true'){
                return  response()->json([
                    'status'=>true,
                ],200);
            }else{
                return  response()->json([
                    'status'=>false,
                ],400);
            }


        }catch (\Exception $e){
            return response()->json(['status'=>$e->getMessage()],400);
        }
    }
}
