<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Sentinel;
use Reminder;
use Mail;

class ForgotPasswordController extends Controller
{
    public function  postForgotPassword(Request $request)
    {
        try{
        $user = User::whereEmail($request->email)->first();
        $sentinelUser = Sentinel::findById($user->id);
        if(count($user)== 0)
            return response()->json([
                'true'=>'email send'
            ],200);
        $reminder = Reminder::exists($sentinelUser) ?: Reminder::create($sentinelUser);
        $this->sendEmail($user,$reminder->code);
        return response()->json([
            'true'=>'email send'
        ],200);
          }catch (\Exception $e){
            return response()->json([
                'true'=>'email send'
            ],200);
        }


    }
    private function sendEmail($user,$code)
    {
        Mail::send('Autentication.forgorPassword', ['user'=>$user,'code'=>$code],function ($message) use ($user){
            $message->to($user->email);
            $message->subject("Hola $user->first_name,reinicia tu password.");
        });
    }
    public function postResetPassword(Request $request){
        $this->validate($request,[
            'password'=>'confirmed|required|min:5|max:50',
            'password_confirmation'=>'required|min:5|max:50'
        ]);
        $email = $request->get('email');
        $resetCode = $request->get('resetCode');
        $user= User::byEmail($email);
        $sentinelUser= Sentinel::findById($user->id);
        if(count($user)== 0 ){
            return response()->json([
                'status'=>false
            ],500);
        }
        if($reminder = Reminder::exists($sentinelUser)){
            if($resetCode == $reminder->code){
                if( Reminder::complete($sentinelUser,$resetCode,$request->password)){
                    return response()->json([
                        'status'=>true
                    ],200);
                }else{
                    return response()->json([
                        'status'=>false
                    ],500);
                }



            }else {
                return response()->json([
                    'status'=>false
                ],500);
            }

        }else{
            return response()->json([
                'status'=>false
            ],500);
        }

    }
}
