<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Mockery\Exception;
use Sentinel;
use Reminder;
use Activation;
class ActivationController extends Controller
{
    public function register(Request $request){
        try{
            $user = Sentinel::register($request->all());
            $role = Sentinel::findRoleBySlug('cliente');
            $role->users()->attach($user);
            $activation = Activation::create($user);
            $reminder = Reminder::exists($user) ?: Reminder::create($user);
            return response()->json(['status'=>true],200);
        } catch(Exception $e){

        }
    }
    public function postActivate( Request $request)
    {
        $this->validate($request,[
            'password'=>'confirmed|required|min:5|max:50',
            'password_confirmation'=>'required|min:5|max:50'
        ]);
        $email = $request->get('email');

        $user = User::whereEmail($email)->first();
        $sentinelUser= Sentinel::findById($user->id);

        if(Activation::complete($sentinelUser,$request->get('activationCode')))
        {

            $user= User::byEmail($email);
            $sentinelUser= Sentinel::findById($user->id);
            if(count($user)== 0 ){
                abort(404);
            }
            if($reminder = Reminder::exists($sentinelUser)){
                $resetCode = $request->get('resetCode');
                if($resetCode = $reminder->code){

                    Reminder::complete($sentinelUser,$resetCode,$request->password);
                    return response()->json(['status'=>true],200);

                }else {
                    return response()->json(['status'=>false],500);
                }
            }else{
                return response()->json(['status'=>false],500);
            }
        }else{
            abort(404);
        }


    }
    private function sendEmail($user,$code,$codeCambio){
        Mail::send('Autenticacion.activation',[
            'user'=>$user,
            'code'=>$code,
            'codeCambio'=>$codeCambio
        ],function($message) use ($user){
            $message->to($user->email);
            $message->subject("Hola $user->first_name, activa tu cuenta.");
        });
    }
}
