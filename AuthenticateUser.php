<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use Illuminate\Support\Facades\Route;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;

class AuthenticateUser extends Controller
{
    public function loginUser(Request $request)
    {
        try{
            //This is for clients token
            if (Sentinel::authenticate($request->all())) {
                if (Sentinel::getUser()->roles()->first()->slug == 'parent') {
                    $request->request->add([
                        'client_secret'=>'KI9p4CqNUUKDVNoDsbuUbjvN11dZeyfBMpEtD23l',
                        'client_id'=> '2',
                        'grant_type'=>'password',
                        'scope' => 'admin',
                        'username'=> $request->get('email')// grant manage order scope for user with admin role
                    ]);
                    $tokenRequest = Request::create(
                        '/oauth/token',
                        'post'
                    );
                    return Route::dispatch($tokenRequest);
                }
                //This is for admin tokens
                if(Sentinel::getUser()->roles()->first()->slug == 'admin'){
                    //Here  we created a new request for Passport's api can make the new  token
                    $request->request->add([
                        'client_secret'=>'KI9p4CqNUUKDVNoDsbuUbjvN11dZeyfBMpEtD23l',
                        'client_id'=> '2',
                        'grant_type'=>'password',
                        'scope' => 'admin',
                        'username'=> $request->get('email')// grant manage order scope for user with admin role
                    ]);
                    //Here we call the api
                    $tokenRequest = Request::create(
                        '/oauth/token',
                        'post'
                    );
                    return Route::dispatch($tokenRequest);
                }
            } else {
                return response()->json([
                    'error'=>'error authentication'
                ],400);
            }
        }catch(ThrottlingException $e){
            //Here is the Exception for the delay that is trigger for multiple fails login
            $delay =  $e->getDelay();
            return response()->json([
                'error'=>'Tu cuenta ha sido bloqueada por : ' .$delay . ' segundos'
            ],400);

        }

    }
    public function logoutUser(){

    }
}
