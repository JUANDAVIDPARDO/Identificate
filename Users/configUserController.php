<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class configUserController extends Controller
{
    public function getRoles(){
        try{
            $roles = DB::table('modules.role')
                ->get();
            return response()->json(['roles'=>$roles],200);
        } catch(Exception $e){
            return response()->json(['error'=>$e->getMessage()],400);
        }
    }
}
