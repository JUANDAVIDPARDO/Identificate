<?php

namespace App\Http\Controllers;

use App\Events\MessagePosted;
use App\Message;
use App\User;
use Illuminate\Http\Request;
use DB;
use Sentinel;
class MensajeController extends Controller
{
    public function index(Request $request)
    {
        if ($request) {

            $mensajes = DB::table('message as mj')
                ->join('users as u', 'mj.user_id', '=', 'u.id')
                ->select('mj.mensaje as message', 'u.first_name AS user')
                ->get();
            return $mensajes;

        }
    }
    public function store(Request $request){
        $user = Sentinel::getUser();
        $user2 = User::byEmail($user->email)->first();

        $mensaje = new Message;
        $mensaje->mensaje = $request->get('message');
        $mensaje->user_id = $user->id;
        $mensaje->save();

        $messages = $user2->messages()->create([
            'message' => request()->get('message')
        ]);
        event(new MessagePosted($mensaje,$user2));

        return ['status'=>$messages];

    }
}
