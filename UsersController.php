<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Sentinel;
use Carbon\Carbon;
use DB;
use Mail;
use Activation;
use Reminder;

class UsersController extends Controller
{
    public function store(Request $request){

        try{

                DB::beginTransaction();


            $request->request->add([
                'password'=>'GmCSIGTBp'

            ]);
                $user = Sentinel::register($request->all());
                if($request->get('rol')== 'parent'){
                    $role = Sentinel::findRoleBySlug('client');
                }
                if($request->get('rol')== 'admin'){
                $role = Sentinel::findRoleBySlug('admin');
               }

                $role->users()->attach($user);

                $activation = Activation::create($user);
                $reminder = Reminder::exists($user) ?: Reminder::create($user);

                $this->sendEmail($user,$activation->code,$reminder->code);
                DB::commit();
                return response()->json(['status'=>true],200);


        }catch(\Exception $e){
            DB::rollback();

            return response()->json(['status'=>$e->getMessage()],400);
        }


    }
    public function getAllUser(){
        $users = DB::table('users')
            ->get();
        return response()->json(['status'=>$users],200);

    }
    public function getUser(Request $request){
        return response()->json(['user'=>$request->user()],200) ;

    }
    private function sendEmail($user,$code,$codeCambio){
        Mail::send('Autentication.activation',[
            'user'=>$user,
            'code'=>$code,
            'codeCambio'=>$codeCambio
        ],function($message) use ($user){
            $message->to($user->email);
            $message->subject("Hola $user->first_name, activa tu cuenta.");
        });
    }
    public function postActivate( Request $request,$email,$activationCode)
    {
        $this->validate($request,[
            'password'=>'confirmed|required|min:5|max:50',
            'password_confirmation'=>'required|min:5|max:50'
        ]);
        $user = User::whereEmail($email)->first();
        $sentinelUser= Sentinel::findById($user->id);

        if(Activation::complete($sentinelUser,$activationCode))
        {

            $user= User::byEmail($email);
            $sentinelUser= Sentinel::findById($user->id);
            if(count($user)== 0 ){
                abort(404);
            }
            if($reminder = Reminder::exists($sentinelUser)){
                if($resetCode = $reminder->code){
                    Reminder::complete($sentinelUser,$resetCode,$request->password);

                    return response()->json(['status'=>true],200);
                }else {
                    return response()->json(['status'=>'incorrect code'],500);
                }
            }else{
                return response()->json(['status'=>false],500);
            }
        }else{
            return response()->json(['status'=>false],500);
        }


    }
}
