<?php

namespace App\Http\Controllers\Modules;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class modulesController extends Controller
{
    public function getAction(Request $request){
        try{

            $module = DB::select('select modules.action_per_module(\'{"modules":['.$request->modules.']}\')');
            if(empty($module) ){

                return  response()->json([
                    'status','no hay registro'


                ],400);
            }else{

                $json = json_decode($module[0]->action_per_module);
                return  response()->json([
                    $json
                ],200);
            }



        }catch (\Exception $e){
            return response()->json(['status'=>$e->getMessage()],400);
        }

    }

    public function getModule(Request $request){
        try{

            $module = DB::select('select modules.module_and_action_per_rol(\'{"roles":['.$request->roles.']}\')');
            if(empty($module) ){

                return  response()->json([
                    'status','no hay registro'


                ],400);
            }else{

                $json = json_decode($module[0]->module_and_action_per_rol);
                return  response()->json([
                    $json
                ],200);
            }



        }catch (\Exception $e){
            return response()->json(['status'=>$e->getMessage()],400);
        }

    }
}
