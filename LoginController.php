<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use DB;
class LoginController extends Controller
{
    public function index(Request $request)
    {
        return view('Autentication.login');
    }
    public function loginUser(Request $request)
    {
        try{
        if (Sentinel::authenticate($request->all())) {

           return  redirect('/chat');
        }
        }catch (\Exception $e){
             dd($e->getMessage());
        }
    }
}
