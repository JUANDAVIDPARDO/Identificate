<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class RegistrationController extends Controller
{
    public function checkPublicForce(Request $request){
        try{

            $profile = DB::connection('mysql_publicForce')->select('SELECT id, first_name, last_name FROM users WHERE card = '.$request->card);
            if(empty($profile) ){

                return  response()->json([
                    'status','no hay registro'
                ],400);
            }else{
                return  response()->json([

                    'profile'=>$profile,

                ],200);
            }


        }catch (\Exception $e){
            return response()->json(['status'=>$e->getMessage()],400);
        }

    }

    public function singInPublicForce(Request $request){
        try{

            $profile = DB::select('INSERT INTO users 
                                           (id_force,
                                            email,
                                            first_name, 
                                            last_name,
                                            card, 
                                            force, 
                                            rol
                                            ) 
                                    VALUES (\''.$request->id.'\', 
                                            \''.$request->email.'\', 
                                            \''.$request->name.'\', 
                                            \''.$request->lastname.'\', 
                                            \''.$request->card.'\', 
                                            \''.$request->force.'\', 
                                            \''.$request->rol.'\' )');

                return  response()->json([

                    'status'=>'success',

                ],200);



        }catch (\Exception $e){
            return response()->json(['status'=>$e->getMessage()],400);
        }

    }

}
